/* global angular */
"use strict";

angular
    .module('snapUp')
    .directive('popupWindow', ['popup', function(popup){
        return {
            restrict: 'E',
            template: '<div class="overlay" ng-class="{opened : popup.isVisible}" ng-include="popup.templateId"></div>',
            replace: true,

            link: function($scope) {
                $scope.isDescriptionClosed = true;
                $scope.closePopUp = function() {
                    popup.isVisible = false;
                    popup.templateId = '';
                };

                $scope.showDescription = function(){
                    $scope.isDescriptionClosed = !$scope.isDescriptionClosed;
                    $('.description-box').slideToggle(300);
                }
            }
        };
    }]);