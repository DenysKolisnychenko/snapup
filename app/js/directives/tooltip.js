/* global angular */
"use strict";

angular
    .module('snapUp')
    .directive('tooltip', function(){
        return {
            restrict: 'A',

            link: function($scope) {
                $('[data-toggle="tooltip"]').tooltip();
            }
        };
    });