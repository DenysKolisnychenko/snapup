/*global angular*/

(function () {
    'use strict';

    function Popup() {
        this.isVisible = false;
        this.templateId = null;
    }

    var templates = {
        create_new_project: 'js/partials/popups/create-new-project.html'
    };

    Popup.prototype.show = function(id) {
        this.isVisible = true;
        this.templateId = templates[id];
    };

    Popup.prototype.close = function () {
        this.isVisible = false;
        this.templateId = null;
    };

    angular
        .module('snapUp')
        .service('popup', Popup);
}());