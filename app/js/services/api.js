/* global angular */

(function(){
    "use strict";

    function Api($http, $q) {
        this._method = 'GET';
        this._URL = 'snapup.json';
        this._http = $http;
        this._q = $q;
    }

    Api.inject = [
        '$http',
        '$q'
    ];

    Api.prototype._getApi = function() {
        return this._http(
            {
                method: this._method,
                url:    this._URL
            }
        );
    };

    Api.prototype.getData = function() {
        return this._getApi().then(
            function(data){ return data.data },
            function(status) {
            console.log('ERROR : ', status);
        });
    };

    angular
        .module('snapUp')
        .service('api', Api);
})();