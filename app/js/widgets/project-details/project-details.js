/* global angular */
"use strict";

angular
    .module('snapUp')
    .directive('projectDetails', function(){
        return {
            templateUrl: 'js/widgets/project-details/project-details.html',
            restrict: 'E',
            replace: true,

            link: function($scope, root) {
                $scope.isDetailsVisible = false;
                $scope.toggleDetails = function(){
                    $scope.isDetailsVisible = !$scope.isDetailsVisible;
                    $(root).parents('.box').toggleClass('overflow');
                }
            }
        };
    });