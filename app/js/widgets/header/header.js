/* global angular */
"use strict";

angular
    .module('snapUp')
    .directive('header', ['popup', function(popup){
        return {
            templateUrl: 'js/widgets/header/header.html',
            restrict: 'A',
            replace: true,

            link: function($scope) {
                $scope.isDropdownVisible = false;
                $scope.isTabledView = true;
                $scope.isListView = false;
                $scope.popup = popup;

                $scope.toggleDropdown = function(){
                    $scope.isDropdownVisible = !$scope.isDropdownVisible;
                };

                $scope.toggleViewStyle = function(){
                    $scope.isTabledView = !$scope.isTabledView;
                    $scope.isListView = !$scope.isListView;
                };

                $scope.showPopup = function(){
                    popup.show('create_new_project');
                }
            }
        };
    }]);