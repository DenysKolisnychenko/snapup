/* global angular */
"use strict";

angular
    .module('snapUp')
    .directive('sidebar', function(){
        return {
            templateUrl: 'js/widgets/sidebar/sidebar.html',
            restrict: 'A',
            replace: true,

            link: function($scope) {
                $scope.isSidebarOpened = false;

                $scope.toggleSidebar = function(){
                    $scope.isSidebarOpened = !$scope.isSidebarOpened;
                }
            }
        };
    });