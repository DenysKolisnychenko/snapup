"use strict";

angular
    .module('snapUp')
    .filter('filterProjects', function() {
        return function(projects, selectedTag) {
            var filteredProjects = [];
            if (projects && projects.length) {

                projects.forEach(function(project) {
                    if (project.id === selectedTag.id) {
                        filteredProjects.push(project);
                    }
                });

                return filteredProjects;
            }
        };
    }
);