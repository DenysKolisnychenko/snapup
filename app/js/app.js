"use strict";

angular
  .module('snapUp', ['ngRoute'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider.
        when('/', {
          templateUrl: 'js/views/index.html',
          controller: 'IndexPageCtrl'
        }).
        otherwise({
            redirectTo: '/'
        });
}]);
