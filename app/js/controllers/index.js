"use strict";

angular
    .module('snapUp')
    .controller('IndexPageCtrl', ['$scope', 'api', function ( $scope, api ) {
        $scope.selectedTag = null;

        moment.locale('en', {
            relativeTime : {
                future: "in %s",
                past:   "%s",
                s:  "seconds ago",
                m:  "a minute ago",
                mm: "%d minutes ago",
                h:  "an hour ago",
                hh: "%d hours ago",
                d:  "yesterday",
                dd: "%d days ago",
                M:  "a month ago",
                MM: "%d months ago",
                y:  "a year ago",
                yy: "%d years ago"
            }
        });


        api.getData().then(function(data) {
            $scope.data = data;
            $scope.selectedTag = $scope.data.tags[0];

            $scope.data.projects.forEach(function(project){
                project.details.convertedDate = transformDate(project.details.date);

                function transformDate(date){
                    return moment(date, "YYYYMMDD").add(11,'h').fromNow();
                }

                project.details.convertedScreens = project.details.screens > 99 ? '99+' : project.details.screens;
            })
        });

        $scope.selectTag = function (tag) {
            $scope.selectedTag = tag;
        };
    }]);